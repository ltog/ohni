# OSM highway name interpolation

A quality assurance tool to find unnamed highways in OSM data whose names can be derived from neighbouring ways and be armchair-mapped with high confidence.


## What is this software about?

Usually, in OpenStreetMap (OSM) roads are tagged with `highway=*` and `name=*`. Sometimes the name of a road is not tagged. We can easily find such roads (e.g. using the [Overpass API](https://overpass-turbo.eu/?Q=way%0A%20%20%5Bhighway%5D%5B!name%5D%0A%20%20(%7B%7Bbbox%7D%7D)%3B%0Aout%20body%3B%0A%3E%3B%0Aout%20skel%20qt%3B)). However, we can't easily add the missing name, since it's hard to know the name without having been there.

This software scans OpenStreetMap files for ways tagged with `highway=*` but missing a `name=*`. Based on some heuristics these ways are filtered additionally: Only those ways are reported where it is assumed that the missing name can be interpolated from neighbouring ways. This is a read-only operation, changes must be done manually!

Let's look at an example:

```
                           Way 2
                           highway = residential
                           (no name)
                         o-----------------------o
                Way 1   /                         \   Way 3
highway = residential  /                           \  highway = residential
   name = Via Vecchia /                             \ name    = Via Vecchia
                     o                               o
```

Since the nameless way 2 is connected to ways 1 and way 3 that both have the same name, it is assumed that way 2 carries also this name.

This software uses the following terminology:

```
                                  gap way
                         o-----------------------o
                        /                         \
           support way /                           \ support way
                      /                             \
                     o                               o
```
- **gap (way)**: a way without name, whose name potentially can be interpolated
- **support (ways)**: ways that are connected to a gap, that define the gap's name
- **named (way)**: every way that has `name=*`, candidate for being a support way

There are a lot of variations regarding the arrangement of gaps and named ways. In order to minimize the number of false positives and non-obvious cases, most corner cases are rejected.

Example of a gap that will not be interpolated:

```
                     Way 2
                     highway = residential
                     (no name)
               o----------o----------------------o
                         /                        \
                Way 1   /                          \   Way 3
highway = residential  /                            \  highway = residential
   name = Via Vecchia /                              \ name    = Via Vecchia
                     o                                o
```

In future versions I might adjust the behaviour or make it configurable. Open an issue on Gitlab if you have a specific need.

## Installation (one-time setup)

```bash
git clone ...
cd ohni
virtualenv -p python3
source venv/bin/activate
pip install -r requirements.txt
deactivate
```


## Example workflow

1. Download an OpenStreetMap data extract from <https://download.geofabrik.de>. (Note: Using the extracts with full metadata is _not_ recommended, since it doesn't provide any advantage.)
   ```bash
   wget http://download.geofabrik.de/europe/switzerland-latest.osm.pbf
   ```
2. Optionally install `osmosis` (see also next step):
   ```bash
   sudo apt install osmosis
   ```
3. Optionally remove irrelevant parts from your data extract (might improve overall performance, but I think it doesn't):
   ```bash
   osmosis --read-pbf switzerland-latest.osm.pbf --tf accept-ways highway=\* --tf reject-relations --used-node --write-pbf switzerland-latest-reduced.osm.pbf`
   ```
4. Enable venv:
   ```bash
   source venv/bin/activate
   ```
5. Run `ohni`:
   ```bash
   ./ohni -i switzerland-latest.osm.pbf -o switzerland-latest.out.osm.pbf
   ```
6. Optionally disable venv:
   ```bash
   deactivate
   ```
7. Open the resulting file in JOSM
8. Optionally enable the map paint style [Coloured Streets](https://josm.openstreetmap.de/wiki/Styles/Coloured_Streets) to JOSM which also is useful when mapping addresses. It assigns one of 33 colours to every way/address. Same streets will receive the same colour.
   ```
   JOSM -> View -> Map Paint Styles -> Map paint preferences -> Coloured Streets -> ⏵
   ```
9. Open the background imagery `OpenStreetMap Carto`
10. Optionally select all objects in order to give them better visibility (Ctrl-a)
11. Zoom to the next way and take note of its name
12. Download the surrounding data (as a new layer!)
13. Make sure that the interpolated name is plausible
14. Add the missing name to the way
15. If you use the `Coloured streets` style, make sure that the colour of the gap way is the same as of the support ways
16. Compare tags of support ways, gap and surroundings and fix/adjust if necessary. Add a note if there are things you can't decide/fix from the comfort of your armchair.
17. Upload your changes (specify `interpolation` as source)
18. Delete downloaded layer
19. Optionally delete the way that you just fixed from the remaining layer and save the file (Ctrl-s).
20. Repeat


## Notes for developers

### Changing which ways are reported back

If you would like to change the ways which `ohni` reports back, adjust the function `interpolate_gap()`. For each gap, it receives the following data:

```plantuml
hide circle
GapInfo o- NamedInfo
class GapInfo <<NamedTuple>>{
    type: str
    wid: int
    nids: List[int]
    is_closed: bool
    appendices: List[NamedInfo]
}
class NamedInfo <<NamedTuple>>{
    tags: Dict[str, str]
    type: str
    name: str
    nids: List[int]
}
```

`GapInfo` contains the information about a specific gap. `GapInfo.appendices` is a list of `NamedInfo`: information about (directly) connected highways carrying a name.

### Running tests / static type checking

Running tests is easy:

    pytest

And so is static type checking:

    mypy --strict ohni.py

## Authors and acknowledgment

The primary author is Lukas Toggenburger (email: ltoggenburgerxxswissonline.ch, replace xx with @)

The software makes heavy use of [pyosmium](https://github.com/osmcode/pyosmium) (written by Sarah Hoffmann), which uses [libosmium](https://github.com/osmcode/libosmium) (written by Jochen Topf).

## License

AGPLv3, contact me if this is not working for you.
