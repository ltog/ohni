#!/usr/bin/env python3

__author__ = "Lukas Toggenburger"
__version__ = "0.0.0"
__license__ = "GPLv3"

import argparse
import os
import osmium as o
import sys
import time
from typing import List, Dict, Set, NamedTuple, Optional

import osmium.osm.mutable

NamedInfo = NamedTuple("NamedInfo",[
    ('tags', Dict[str, str]),
    ('type', str),
    ('name', str),
    ('nids', List[int]),
])

GapInfo = NamedTuple("GapInfo",[
    ('type', str),
    ('wid', int),
    ('nids', List[int]),
    ('is_closed', bool),
    ('appendices', List[NamedInfo])
])

def main(args: argparse.Namespace) -> None:
    # given a node id, find ids of (named) ways that are spanned by this node
    nid2named_wids: Dict[int, Set[int]] = dict()
    gaps: List[GapInfo] = list()
    nameds: Dict[int, NamedInfo] = dict()

    interpolated_wid2name: Dict[int, str] = dict()
    interpolated_nids: Set[int] = set()

    if os.path.exists(args.outfile):
        print(f"Output file '{args.outfile}' exists. Aborting...",
              file=sys.stderr)
        sys.exit(1)

    print('Finding highways without names ...')
    begin = time.time()
    begin_overall = begin
    h1: Handler1 = Handler1(args, gaps, nameds, nid2named_wids)
    h1.apply_file(args.infile)
    end = time.time()
    print_time(begin, end)

    print('Appending appendices...')
    begin = time.time()
    append_appendices(gaps, nameds, nid2named_wids)
    end = time.time()
    print_time(begin, end)

    print('Interpolating gaps...')
    begin = time.time()
    interpolate_gaps(gaps, interpolated_wid2name, interpolated_nids, args)
    end = time.time()
    print_time(begin, end)

    num_matches = len(interpolated_wid2name)

    print('Writing output file...')
    begin = time.time()
    h2 = Handler2(args, interpolated_wid2name, interpolated_nids)
    h2.apply_file(args.infile, locations=True)
    end = time.time()
    print_time(begin, end)

    print("\nSummary")
    print(f'Found {num_matches} match(es).')
    print_time(begin_overall, end)


def append_appendices(gaps: List[GapInfo],
                      nameds: Dict[int, NamedInfo],
                      nid2named_wids: Dict[int, Set[int]]) -> None:
    for gap in gaps:
        connected_way_ids = set()
        for nid in gap.nids:
            if nid in nid2named_wids:
                wids = nid2named_wids[nid]
                for wid in sorted(wids):
                    connected_way_ids.add(wid)
        for wid in connected_way_ids:
            gap.appendices.append(nameds[wid])


def interpolate_gaps(gaps: List[GapInfo],
                     interpolated_wid2name: Dict[int, str],
                     interpolated_nids: Set[int],
                     args: argparse.Namespace) -> None:
    for gap in gaps:
        name = interpolate_gap(gap, args.allow_differing_support_tags)

        if name:
            interpolated_wid2name.update({gap.wid: name})
            interpolated_nids.update(gap.nids)


def interpolate_gap(gap: GapInfo,
                    allow_differing_support_tags: str) -> Optional[str]:
    """Determine whether a gap can be interpolated.

    If a gap can be interpolated, return its name (str).
    Otherwise, return None.
    """
    if gap.is_closed:
        return None

    # count name occurrences in appendices
    name_count: Dict[str, int] = dict()
    for appendix in gap.appendices:
        if appendix.name not in name_count:
            name_count[appendix.name] = 0
        name_count[appendix.name] += 1

    # filter names that occur exactly twice
    names_occurring_twice: List[str] =\
        [k for k, v in name_count.items() if v == 2]

    # we want exactly one name that occurs twice
    if len(names_occurring_twice) != 1:
        return None

    # get the missing name of this gap
    name: str = names_occurring_twice[0]

    # extract the two support ways
    support_ways = list() # will have length 2
    for appendix in gap.appendices:
        if appendix.name == name:
            support_ways.append(appendix)

    # gap and support ways must be of the same type, e.g. primary
    # this is actually checked twice (again when comparing tags)
    # TODO: delete .type ?
    for i in (0, 1):
        if gap.type != support_ways[i].type:
            return None

    # support ways must not intersect each other
    if not set(support_ways[0].nids).isdisjoint(set(support_ways[1].nids)):
        return None

    common_nodes = set() # nodes shared by gap and support ways
    for i in (0, 1):
        common_nodes.update(set(support_ways[i].nids).intersection(set(gap.nids)))

    # the first and last gap node must be a common node
    for i in (0, -1):
        if gap.nids[i] not in common_nodes:
            return None

    # support ways must join gap on different ends
    if len(common_nodes) != 2:
        return None

    # support ways must have the same tags
    tags_differ: bool = (support_ways[0].tags != support_ways[1].tags)
    if allow_differing_support_tags == 'no' and tags_differ:
        return None
    elif allow_differing_support_tags == 'only' and not tags_differ:
        return None

    # gaps must be connected to the first/last node of the support ways
    for i in (0, 1):
        if support_ways[i].nids[0] not in common_nodes and \
                support_ways[i].nids[-1] not in common_nodes:
            return None

    return name


def print_time(begin: float, end: float) -> None:
    duration = end - begin
    print("Took %.2f seconds." % duration)


class Handler1(o.SimpleHandler):
    def __init__(self,
                 args: argparse.Namespace,
                 gaps: List[GapInfo],
                 nameds: Dict[int, NamedInfo],
                 nid2named_wids: Dict[int, Set[int]]):
        super(Handler1, self).__init__()
        self.args = args
        self.gaps = gaps
        self.nameds = nameds
        self.nid2named_wids = nid2named_wids

    def way(self, w: o.osm.Way) -> None:
        highway = w.tags.get('highway')
        name = w.tags.get('name')

        # this name is not part of the highway network, therefore irrelevant
        if highway is None:
            return

        if name is None: # it's a gap
            gap_type = highway
            gap_wid: int = w.id
            gap_nids: List[int] = list()
            for n in w.nodes:
                gap_nids.append(n.ref)
            gap_is_closed = w.is_closed()
            gap = GapInfo(gap_type, gap_wid, gap_nids, gap_is_closed, list())
            self.gaps.append(gap) #
        else: # it's a named way #TODO: memory-saving option: do this in a later handler
            named_tags = {k:v for (k,v) in w.tags}
            named_name = name
            named_type = highway
            named_wid: int = w.id
            named_nids: List[int] = list()
            for n in w.nodes:
                named_nids.append(n.ref)
                # does id of current node exist as key?
                if n.ref not in self.nid2named_wids:
                    self.nid2named_wids[n.ref] = set()
                self.nid2named_wids[n.ref].add(named_wid)
            named = NamedInfo(named_tags, named_type, named_name, named_nids)
            self.nameds.update({named_wid: named})


class Handler2(o.SimpleHandler):
    def __init__(self,
                 args: argparse.Namespace,
                 interpolated_wid2name: Dict[int, str],
                 interpolated_nids: Set[int]):
        super(Handler2, self).__init__()
        self.args = args
        self.interpolated_wid2name = interpolated_wid2name
        self.interpolated_nids = interpolated_nids
        self.writer = o.SimpleWriter(args.outfile)

    def node(self, n: o.osm.Node) -> None:
        if n.id in self.interpolated_nids:
            self.writer.add_node(n)
            # not sure if it helps or hurts performance
            self.interpolated_nids.remove(n.id)

    def way(self, w: o.osm.Way) -> None:
        if w.id in self.interpolated_wid2name:
            new_tags = dict()
            for tag in w.tags:
                new_tags[tag.k] = tag.v
            new_tags['name'] = self.interpolated_wid2name[w.id]
            new_way = osmium.osm.mutable.Way(base=w, tags=new_tags)

            self.writer.add_way(new_way)
            # not sure if it helps or hurts performance
            self.interpolated_wid2name.pop(w.id)


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser(
        description='Find OSM ways tagged as `highway=*` without `name=*` '
                    'whose name can be interpolated from neighbouring ways.')
    parser.add_argument('-i', '--infile',
                        type=str, help='The file to be analyzed', required=True)
    parser.add_argument('-o', '--outfile',
                        type=str,
                        help='The file to write results into (filetype will be'
                             ' determined by filename extension)',
                        required=True)
    parser.add_argument('--allow-differing-support-tags',
                        type=str,
                        help='',
                        choices=['yes', 'no', 'only'],
                        default='no')
    parser.add_argument('-v', '--verbose', help='Write more output',
                        action='store_true')
    # TODO: allow to specify which highway=* to look at
    # TODO: output csv
    # TODO: skip last handler by saving osm objects before
    # TODO: handle closed gaps/support ways
    args: argparse.Namespace = parser.parse_args()
    main(args)
