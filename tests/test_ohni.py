import argparse
import ohni
import osmium
import pytest

from collections import namedtuple

OsmIds = namedtuple('OsmIds', ['nodes', 'ways', 'relations'])

@pytest.fixture
def args(tmp_path):
    args = argparse.Namespace
    args.infile = 'this_needs_to_be_replaced_by_individual_test_functions'
    args.outfile = str(tmp_path / 'tmp.osm.pbf')
    args.allow_differing_support_tags = 'no'
    args.verbose = False
    return args


class OsmStats:
    def __init__(self, file: str):
        self.ids = self.ids = OsmIds(nodes=list(), ways=list(),
                                     relations=list())

        class Handler(osmium.SimpleHandler):
            def __init__(self, ids):
                super(Handler, self).__init__()
                self.ids = ids

            def node(self, n: osmium.osm.Node):
                self.ids.nodes.append(n.id)

            def way(self, w: osmium.osm.Way):
                self.ids.ways.append(w.id)

            def relation(self, r: osmium.osm.Relation):
                self.ids.relations.append(r.id)

        h = Handler(self.ids)
        h.apply_file(file)

        print(f'str(self.ids.ways)={str(self.ids.ways)}')

    def contains_node(self, node):
        return self.contains_nodes([node])

    def contains_nodes(self, nodes):
        for node in nodes:
            if node not in self.ids.nodes:
                print(f'Fail for node id {node}')
                return False
        return True

    def not_contains_nodes(self, nodes):
        for node in nodes:
            if node in self.ids.nodes:
                print(f'Fail for node id {node}')
                return False
        return True

    def contains_way(self, way):
        return self.contains_ways([way])

    def contains_ways(self, ways):
        for way in ways:
            if way not in self.ids.ways:
                print(f'Fail for way id {way}')
                return False
        return True

    def not_contains_ways(self, ways):
        for way in ways:
            if way in self.ids.ways:
                print(f'Fail for way id {way}')
                return False
        return True

    def contains_relation(self, relation):
        return self.contains_relatiosn([relation])

    def contains_relations(self, relations):
        for relation in relations:
            if relation not in self.ids.relations:
                print(f'Fail for relation id {relation}')
                return False
        return True

    def not_contains_relations(self, relations):
        for relation in relations:
            if relation in self.ids.relations:
                print(f'Fail for relation id {relation}')
                return False
        return True

    def contains_no_nodes(self):
        return not self.ids.nodes

    def contains_no_ways(self):
        return not self.ids.ways

    def contains_no_relations(self):
        return not self.ids.relations


def test_01(args):
    """
    A most simple case: A nameless way between two disjoint ways that
    carry the same name.
    """
    args.infile = 'tests/01.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    # this way and these nodes are part of the correct result
    assert stats.contains_way(100)
    assert stats.contains_nodes([1, 2, 3, 4, 5])

    # these ways are support ways and should not be included in the result
    assert stats.not_contains_ways([200, 300])

    # these nodes are part of support ways (and not connected to gap way)
    assert stats.not_contains_nodes([21, 22, 23, 31, 32, 33])

    # these are node ids and should not occur as way ids
    assert stats.not_contains_ways([1, 2, 3, 4, 5, 21, 22, 23, 31, 32, 33])

    # these are ways ids and should not occur as node ids
    assert stats.not_contains_nodes([100, 200, 300])


def test_02(args):
    """
    If two support ways share a node, don't count it as valid
    interpolation.
    """
    args.infile = 'tests/02.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_03(args):
    """
    Do not count a way as interpolatable if its end nodes span a
    parallel way with the considered name.
    """
    args.infile = 'tests/03.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_04(args):
    """
    If two support ways connect to the same gap node, do not regard it
    as valid interpolation.
    """
    args.infile = 'tests/04.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_04_01(args):
    """
    If two support ways connect to the same gap node, do not regard it
    as valid interpolation. (One additional unnamed way.)
    """
    args.infile = 'tests/04_01.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_04_02(args):
    """
    If two support ways connect to the same gap node, do not regard it
    as valid interpolation. (Two additional unnamed ways.)
    """
    args.infile = 'tests/04_02.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_04_03(args):
    """
    If two support ways connect to the same gap node, do not regard it
    as valid interpolation. (Example taken from real data.)
    """
    args.infile = 'tests/04_03.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_05(args):
    """
    Only consider a gap interpolatable if the first/last node of the
    support ways connect to the gap.
    """
    args.infile = 'tests/05.osm.xml'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_no_ways()


def test_06_no(args):
    """
    Test argument `--allow-differing-support-tags no`
    """
    args.infile = 'tests/06.osm.xml'
    args.allow_differing_support_tags = 'no'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_way(100)


def test_06_yes(args):
    """
    Test argument `--allow-differing-support-tags yes`
    """
    args.infile = 'tests/06.osm.xml'
    args.allow_differing_support_tags = 'yes'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_ways([100, 400])


def test_06_only(args):
    """
    Test argument `--allow-differing-support-tags only`
    """
    args.infile = 'tests/06.osm.xml'
    args.allow_differing_support_tags = 'only'
    ohni.main(args)
    stats = OsmStats(args.outfile)

    assert stats.contains_way(400)
